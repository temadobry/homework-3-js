// 1.циклы нужны для того, чтобы  производить повторяющееся действие многократно.
// 2. for делает действие в фиксированном диапазоне, а while - цикл идет до наступления какого-то события
// 3. явное приведение проводится через функцию, неявное через знаки. Плюс явных  - более предсказуемое поведение, неявных - меньше кода.


let userNumber = +prompt("Введите число");

while (!Number.isInteger(userNumber)) {
    alert("НЕ целое число!");
    userNumber = +prompt("Введите число")}

if (userNumber < 5 || isNaN(userNumber)) {
    console.log('Sorry, no numbers');
   
}


else  {  
    let i = userNumber
    while (i > 4) {
        if (i % 5 === 0) {
        console.log( i );
        }
      --i;
    }
}

